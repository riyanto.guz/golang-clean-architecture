package controller

import (
	"clean-architecture/dto"
	"clean-architecture/helper"
	"clean-architecture/repository"
	"clean-architecture/util"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type UserController interface {
	FindAll(c echo.Context) error
	GetUser(c echo.Context) error
	UpdateUser(c echo.Context) error
	DeleteUser(c echo.Context) error
}

type userController struct {
	Repository repository.UserRepository
	Validation util.ValidationUtil
}

func NewUserController(repo repository.UserRepository) UserController {
	return &userController{
		Repository: repo,
		Validation: util.NewValidationUtil(),
	}
}

func (u *userController) FindAll(c echo.Context) error {
	users, err := u.Repository.FindAll()
	if err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "User does not exist.",
		})
	}

	var res []dto.UserResponse
	for _, user := range *users {
		res = append(res, dto.UserResponse{
			Id:        user.Id,
			Username:  user.Username,
			Name:      user.Name,
			Email:     user.Email,
			CreatedAt: user.CreatedAt,
			UpdatedAt: user.UpdatedAt,
		})
	}

	return c.JSON(http.StatusOK, helper.Response{
		Status:  http.StatusOK,
		Message: "Success",
		Data:    res,
	})
}

func (u *userController) GetUser(c echo.Context) error {
	id := c.Param("id")
	user, err := u.Repository.GetUser(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "User does not exist.",
		})
	}

	res := dto.UserResponse{
		Id:        user.Id,
		Username:  user.Username,
		Name:      user.Name,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
	}

	return c.JSON(http.StatusOK, helper.Response{
		Status:  http.StatusOK,
		Message: "Success",
		Data:    res,
	})
}

func (u *userController) UpdateUser(c echo.Context) error {
	var req dto.UserRequest
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	if err := u.Validation.Validate(req); err != nil {
		for _, fieldErr := range err.(validator.ValidationErrors) {
			return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
				Status:  http.StatusBadRequest,
				Message: "Something went wrong. Please try again.",
				Errors:  u.Validation.ErrorMessage(fieldErr),
			})
		}
	}

	id := c.Param("id")
	user, err := u.Repository.UpdateUser(req, id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	res := dto.UserResponse{
		Id:        user.Id,
		Username:  user.Username,
		Name:      user.Name,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
	}

	return c.JSON(http.StatusOK, helper.Response{
		Status:  http.StatusOK,
		Message: "Your account has been updated.",
		Data:    res,
	})
}

func (u *userController) DeleteUser(c echo.Context) error {
	id := c.Param("id")
	if isAdmin := u.Repository.IsAdmin(id); isAdmin {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "Admin user not allowed to delete.",
		})
	}

	user, err := u.Repository.DeleteUser(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	res := dto.UserResponse{
		Id:       user.Id,
		Username: user.Username,
		Name:     user.Name,
		Email:    user.Email,
	}

	return c.JSON(http.StatusOK, helper.Response{
		Status:  http.StatusOK,
		Message: "Your account has been deleted.",
		Data:    res,
	})
}
