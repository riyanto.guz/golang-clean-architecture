package controller

import (
	"clean-architecture/dto"
	"clean-architecture/helper"
	"clean-architecture/model"
	"clean-architecture/repository"
	"clean-architecture/util"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type AuthController interface {
	Register(c echo.Context) error
	Login(c echo.Context) error
}

type authController struct {
	Repository repository.AuthRepository
	Validation util.ValidationUtil
	JWT        util.JWTUtil
	Encrypt    util.EncryptUtil
}

func NewAuthController(repo repository.AuthRepository) AuthController {
	return &authController{
		Repository: repo,
		Validation: util.NewValidationUtil(),
		JWT:        util.NewJWTUtil(),
		Encrypt:    util.NewEncryptUtil(),
	}
}

func (a *authController) Register(c echo.Context) error {
	var req dto.RegisterRequest
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	if err := a.Validation.Validate(req); err != nil {
		for _, fieldErr := range err.(validator.ValidationErrors) {
			return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
				Status:  http.StatusBadRequest,
				Message: "Something went wrong. Please try again.",
				Errors:  a.Validation.ErrorMessage(fieldErr),
			})
		}
	}

	user := model.User{
		Username: req.Username,
		Name:     req.Name,
		Email:    req.Email,
		Password: a.Encrypt.HashAndSalt([]byte(req.Password)),
	}

	if a.Repository.IsDuplicateUsername(user.Username) {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "Username already exist.",
		})
	}

	if a.Repository.IsDuplicateEmail(user.Email) {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "Email already exist.",
		})
	}

	if err := a.Repository.Register(&user); err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	res := dto.RegisterResponse{
		Id:        user.Id,
		Username:  user.Username,
		Name:      user.Name,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
	}

	return c.JSON(http.StatusCreated, helper.Response{
		Status:  http.StatusCreated,
		Message: "Thanks for signing up. Your account has been created.",
		Data:    res,
	})
}

func (a *authController) Login(c echo.Context) error {
	var req dto.LoginRequest
	if err := c.Bind(&req); err != nil {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  err.Error(),
		})
	}

	if err := a.Validation.Validate(req); err != nil {
		for _, fieldErr := range err.(validator.ValidationErrors) {
			return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
				Status:  http.StatusBadRequest,
				Message: "Something went wrong. Please try again.",
				Errors:  a.Validation.ErrorMessage(fieldErr),
			})
		}
	}

	user, err := a.Repository.Login(req.Username)
	if err != nil {
		return c.JSON(http.StatusNotFound, helper.ErrorResponse{
			Status:  http.StatusNotFound,
			Message: "Something went wrong. Please try again.",
			Errors:  "Invalid username or password.",
		})
	}

	if checkPwd := a.Encrypt.VerifyPassword(user.Password, []byte(req.Password)); !checkPwd {
		return c.JSON(http.StatusBadRequest, helper.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: "Something went wrong. Please try again.",
			Errors:  "Invalid username or password",
		})
	}

	jwtToken := a.JWT.GenerateToken(user.Id)

	res := dto.LoginResponse{
		Username: user.Username,
		Name:     user.Name,
		Email:    user.Email,
		Token:    jwtToken,
	}

	return c.JSON(http.StatusOK, helper.Response{
		Status:  http.StatusOK,
		Message: "You are successfully logged in.",
		Data:    res,
	})
}
