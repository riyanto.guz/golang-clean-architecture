package middleware

import (
	"clean-architecture/helper"
	"clean-architecture/util"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

const (
	authorizationHeaderKey  = "authorization"
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

func JWTAuth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		authorizationHeader := c.Request().Header.Get(authorizationHeaderKey)

		if len(authorizationHeader) == 0 {
			err := errors.New("Authorization header is not provided.")
			return c.JSON(http.StatusUnauthorized, helper.ErrorResponse{
				Status:  http.StatusUnauthorized,
				Message: "Unauthorized",
				Errors:  err.Error(),
			})
		}

		fields := strings.Fields(authorizationHeader)
		if len(fields) < 2 {
			err := errors.New("Invalid authorization header format.")
			return c.JSON(http.StatusUnauthorized, helper.ErrorResponse{
				Status:  http.StatusUnauthorized,
				Message: "Unauthorized",
				Errors:  err.Error(),
			})
		}

		authorizationType := strings.ToLower(fields[0])
		if authorizationType != authorizationTypeBearer {
			err := fmt.Errorf("Unsupported authorization type %s", authorizationType)
			return c.JSON(http.StatusUnauthorized, helper.ErrorResponse{
				Status:  http.StatusUnauthorized,
				Message: "Unauthorized",
				Errors:  err.Error(),
			})
		}

		accessToken := fields[1]
		jwtToken := util.NewJWTUtil()
		_, err := jwtToken.VerifyToken(accessToken)
		if err != nil {
			err := errors.New("Invalid token.")
			return c.JSON(http.StatusUnauthorized, helper.ErrorResponse{
				Status:  http.StatusUnauthorized,
				Message: "Unauthorized",
				Errors:  err.Error(),
			})
		}

		return next(c)
	}
}
