package main

import (
	"clean-architecture/config"
	"clean-architecture/handler"
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Failed to load env file.")
	}
}

func main() {
	db := config.SetupDatabaseConnection()
	defer config.CloseDatabaseConnection(db)

	e := echo.New()

	v1 := e.Group("/api")

	authHandler := handler.NewAuthHandler(db)
	authHandler.Handler(v1)

	userHandler := handler.NewUserHandler(db)
	userHandler.Handler(v1)

	// list of registered route path
	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	ioutil.WriteFile("./routes.json", data, 0644)

	e.Logger.Fatal(e.Start(":9090"))
}
