package model

import "time"

type User struct {
	Id        int       `gorm:"primaryKey;autoIncrement"`
	Username  string    `gorm:"->;<-:create;type:varchar(255);unique;not null"`
	Name      string    `gorm:"type:varchar(255);not null"`
	Email     string    `gorm:"type:varchar(255);unique;not null"`
	Role      string    `gorm:"type:varchar(255)"`
	Password  string    `gorm:"type:varchar(255);not null"`
	CreatedAt time.Time `gorm:"type:timestamp(0);autoCreateTime"`
	UpdatedAt time.Time `gorm:"type:timestamp(0);autoUpdateTime"`
}
