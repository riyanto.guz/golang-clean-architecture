package repository

import (
	"clean-architecture/model"
	"fmt"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type UserRepository interface {
	FindAll() (*[]model.User, error)
	GetUser(id string) (*model.User, error)
	UpdateUser(req interface{}, id string) (*model.User, error)
	DeleteUser(id string) (*model.User, error)
	IsAdmin(id string) bool
}

type userRepository struct {
	DB *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{
		DB: db,
	}
}

func (r *userRepository) FindAll() (*[]model.User, error) {
	var user []model.User
	if err := r.DB.Table("users").Order("id desc").Find(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *userRepository) GetUser(id string) (*model.User, error) {
	var user model.User
	if err := r.DB.Table("users").Where("id = ?", id).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *userRepository) UpdateUser(req interface{}, id string) (*model.User, error) {
	var user model.User
	if err := r.DB.Table("users").Clauses(clause.OnConflict{DoNothing: true}).Model(user).Where("id = ?", id).Updates(req); err.Error != nil {
		return nil, err.Error
	}

	if err := r.DB.Table("users").Where("id = ?", id).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *userRepository) DeleteUser(id string) (*model.User, error) {
	var user model.User
	if res := r.DB.Table("users").Clauses(clause.Returning{}).Where("id = ?", id).Delete(&user); res.RowsAffected < 1 {
		return nil, fmt.Errorf("User does not exist.")
	}
	return &user, nil
}

func (r *userRepository) IsAdmin(id string) bool {
	var user model.User
	if err := r.DB.Table("users").Where("id = ? AND role = ?", id, "Admin").First(&user).Error; err != nil {
		return false
	}
	return true
}
