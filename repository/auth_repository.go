package repository

import (
	"clean-architecture/model"

	"gorm.io/gorm"
)

type AuthRepository interface {
	Register(user *model.User) error
	Login(username string) (*model.User, error)
	IsDuplicateUsername(username string) bool
	IsDuplicateEmail(email string) bool
}

type authReposiitory struct {
	DB *gorm.DB
}

func NewAuthRepository(db *gorm.DB) AuthRepository {
	return &authReposiitory{
		DB: db,
	}
}

func (r *authReposiitory) Register(user *model.User) error {
	return r.DB.Table("users").Debug().Create(&user).Error
}

func (r *authReposiitory) Login(username string) (*model.User, error) {
	var user model.User
	if err := r.DB.Table("users").Where("username = ?", username).Or("email = ?", username).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *authReposiitory) IsDuplicateUsername(username string) bool {
	var user model.User
	if err := r.DB.Table("users").Where("username = ?", username).First(&user).Error; err != nil {
		return false
	}
	return true
}

func (r *authReposiitory) IsDuplicateEmail(email string) bool {
	var user model.User
	if err := r.DB.Table("users").Where("email = ?", email).First(&user).Error; err != nil {
		return false
	}
	return true
}
