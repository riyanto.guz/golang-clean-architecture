package handler

import (
	"clean-architecture/controller"
	"clean-architecture/repository"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type AuthHandler interface {
	Handler(c *echo.Group)
}

type authHandler struct {
	DB *gorm.DB
}

func NewAuthHandler(db *gorm.DB) AuthHandler {
	return &authHandler{
		DB: db,
	}
}

func (a *authHandler) Handler(c *echo.Group) {
	authRepository := repository.NewAuthRepository(a.DB)
	authController := controller.NewAuthController(authRepository)

	auth := c.Group("/auth")
	{
		auth.POST("/register", authController.Register)
		auth.POST("/login", authController.Login)
	}
}
