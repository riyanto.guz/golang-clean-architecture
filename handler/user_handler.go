package handler

import (
	"clean-architecture/controller"
	"clean-architecture/middleware"
	"clean-architecture/repository"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type UserHandler interface {
	Handler(c *echo.Group)
}

type userHandler struct {
	DB *gorm.DB
}

func NewUserHandler(db *gorm.DB) UserHandler {
	return &userHandler{
		DB: db,
	}
}

func (u *userHandler) Handler(c *echo.Group) {
	userRepository := repository.NewUserRepository(u.DB)
	userController := controller.NewUserController(userRepository)

	user := c.Group("/user")
	{
		user.Use(middleware.JWTAuth)
		user.GET("", userController.FindAll)
		user.GET("/:id", userController.GetUser)
		user.PUT("/:id", userController.UpdateUser)
		user.DELETE("/:id", userController.DeleteUser)
	}
}
